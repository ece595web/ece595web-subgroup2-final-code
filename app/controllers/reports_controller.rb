class ReportsController < ApplicationController
  before_action :set_report, only: [:show, :edit, :update, :destroy, :submit_report, :submit_review]
  before_action :authenticate_user!
  before_filter :require_permission, only: [:new,:create]

  # GET /reports
  # GET /reports.json
  def index
    if current_user.role_type == 'Advisor'
      @reports = Report.where('user_id=?',current_user.id).order('updated_at DESC')
    elsif current_user.role_type == 'Director'
      @reports = Report.where('college_id =? AND department_id=? AND status IN(?)',current_user.college_id,current_user.department_id,['Completed','Submitted']).order('updated_at DESC')
    else
      @reports = Report.all.order('updated_at DESC')
    end
  end

  # GET /reports/1
  # GET /reports/1.json
  def show
  end

  # GET /reports/new
  def new
    @report = Report.new
  end

  # GET /reports/1/edit
  def edit
   @departments = @report.college_id.present? ? Department.where('college_id=?',@report.college_id) : []
   @courses = @report.department_id.present? ? Course.where('department_id=?',@report.department_id) : []
  end

  # POST /reports
  # POST /reports.json
  def create
    @report = Report.new(report_params)
    #@reports_cols = params[:report].any?{|k,v| v.nil?}
    #@goals_cols = params["report"]["goals_attributes"].present? ? params["report"]["goals_attributes"].values.first.any?{|k,v| v.empty?} : false
    
    #@report.status = @reports_cols && @goals_cols ? 'Completed' : 'Incomplete'
    @report.user_id = current_user.id
    @report.end_period = params[:report][:end_period]
    #["report"]["goals_attributes"].values.first["learning_goals"] = ["report"]["goals_attributes"].values.first["learning_goals"].join(",")
    
    respond_to do |format|
      if @report.save
      	update_status(@report)
        format.html { redirect_to reports_url, notice: 'Report was successfully created.' }
        format.json { render :show, status: :created, location: @report }
      else
        format.html { render :new }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reports/1
  # PATCH/PUT /reports/1.json
  def update
    respond_to do |format|
      #@reports_cols = params[:report].any?{|k,v| v.nil?}
    #@goals_cols = params["report"]["goals_attributes"].present? ? params["report"]["goals_attributes"].values.first.any?{|k,v| v.empty?} : false
    
    #@report.status = @reports_cols && @goals_cols ? 'Completed' : 'Incomplete'
      if @report.update(report_params)
      	update_status(@report)
        format.html { redirect_to reports_url, notice: 'Report was successfully saved.' }
        format.json { render :show, status: :ok, location: @report }
      else
        format.html { render :edit }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reports/1
  # DELETE /reports/1.json
  def destroy
    @report.destroy
    respond_to do |format|
      format.html { redirect_to reports_url, notice: 'Report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def update_status(report)
  	r_status = report.college_id.present? && report.department_id.present? && report.course_id.present? && report.assessment_start_period.present? && report.end_period.present? && report.state_all_slos.present? && report.improvements_on_report_period.present?
    p '@@@@@@'
    p r_status
    if report.goals.present?
      goal = report.goals.first
      g_status = goal.outcome.present? && goal.learning_goals.present? && goal.strategic_plan.present? && goal.measure_type.present? && goal.performance_benchmark.present? && goal.data_results.present? && goal.data_analysis.present? && goal.recommendations.present? && goal.met_details.present?
    end
     p g_status
      g_status = report.goals.present? ? g_status : false
      p g_status
      @status = r_status && g_status ? 'Completed' : 'Incomplete'
      report.update_attributes(:status => @status)
  end

  def get_departments_list
  @departments = Department.where('college_id=?',params[:college_id])
  @department_id = @departments.first.id
  @courses = Course.where('department_id=?',@department_id)
  end

  def get_courses_list
    @courses = Course.where('department_id=?',params[:department_id])
  end

  def submit_report
   @report.update_attributes(:status=>'Submitted')
   respond_to do |format|
      format.html { redirect_to reports_url, notice: 'Report was successfully Submitted.' }
      format.json { head :no_content }
    end
  end

  def search
  	@reports = Report.search(params[:report])
  end

  def submit_review
  	@report.update_attributes(:comments => params[:report][:comments], :score => params[:report][:score])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_report
      @report = Report.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def report_params
      params.require(:report).permit(:college_id, :department_id, :course_id, :assessment_start_period, :end_period, :submitted_date, :status, :state_all_slos, :focus_on_slos, :improvements_on_report_period, :user_id,:comments,:score,:avatar,
      	goals_attributes: [:id, :outcome,:learning_goals, :strategic_plan,:measure_type,:performance_benchmark,:data_results,:data_analysis,:recommendations,:met_details,:report_id,:_destroy])
    end

	def require_permission
	  if user_role != 'Advisor'
	    respond_to do |format|
	      format.html { redirect_to reports_url, notice: "You can't create reports." }
	    end
	  else
	   	return true
	  end
	end
end
