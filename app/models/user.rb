class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validate :charge_xor_payment

  #validates :college, :presence => true

  belongs_to :college
  belongs_to :department  

  private

    def charge_xor_payment
      if role_type == 'Director'
        if college_id.blank?
        errors.add(:base, "Please select college and Department!")
        end
      end
    end
end
