class Report < ActiveRecord::Base
belongs_to :college
belongs_to :department
belongs_to :course
has_attached_file :avatar#, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
#validates_attachment_content_type :avatar, content_type: ["image/jpeg", "image/gif", "image/png"]
do_not_validate_attachment_file_type :avatar
has_many :goals
accepts_nested_attributes_for :goals,allow_destroy: true

#serialize :learning_goals, :strategic_plan

def self.search(params)
  #if params.any?{|k,v| v.empty?}
  #	self.all
  #else
  params[:college_id] = params[:college_id].present? ? params[:college_id] : nil
  params[:deparment_id] = params[:department_id].present? ? params[:department_id] : nil
  params[:course_id] = params[:course_id].present? ? params[:course_id] : nil
  params[:year] = params[:year].present? ? params[:year] : nil
  
  if params[:role] == 'Director'
    self.where('course_id =? AND extract(year from created_at) = ?',params[:course_id],params[:year])  
  else
  	self.where('college_id = ? AND department_id = ? AND course_id =? AND extract(year from created_at) = ?',params[:college_id], params[:deparment_id],params[:course_id],params[:year])
  end
end
end
