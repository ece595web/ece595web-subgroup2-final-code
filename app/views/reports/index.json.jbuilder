json.array!(@reports) do |report|
  json.extract! report, :id, :college_id, :department_id, :course_id, :assessment_start_period, :assessment_end_period, :submitted_date, :status, :student_learning_utcomes, :unm_learning_goals, :unm_strategic_plan, :assessment_measures, :performance_benchmark, :data_results, :data_analysis, :recommendations, :met_details
  json.url report_url(report, format: :json)
end
