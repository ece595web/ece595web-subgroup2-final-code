# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
   User.create(email: 'admin@unm.edu', password: 'admin12345', password_confirmation: 'admin12345', role_type: 'Admin')
  
   colleges = College.create([{ name: 'University of New Mexico - Main Campus' }, { name: 'University of New Mexico - North Campus' }, { name: 'University of New Mexico - South Campus' }])
   departments = Department.create([{ name: 'Department Of Engineering',college_id: '1' }, { name: 'Department Of Medical Science',college_id: '1' },{ name: 'School Of Management',college_id: '1' },{ name: 'Department of Arts',college_id: '2' }, { name: 'Department of Foreign Languages',college_id: '2' },  { name: 'Department of Cultural Sciences',college_id: '3' }])
    Course.create([{ name: 'Computer Science',department_id: '1' }, { name: 'Electronics and Communications',department_id: '1' }, { name: 'Computer Engineering',department_id: '1' }, { name: 'General Medicine',department_id: '2' }, { name: 'House Surgeon',department_id: '2' },{ name: 'French',department_id: '5' },{ name: 'Spanish',department_id: '5' },{ name: 'Animation',department_id: '4' },{ name: 'Painting',department_id: '4' },{ name: 'Management of Information Science',department_id: '3' },{ name: 'Marketing Management',department_id: '3' },{ name: 'Dance',department_id: '6' },{ name: 'Music',department_id: '6' }])
