class AddExtraDataToUsers < ActiveRecord::Migration
  def change
    add_column :users, :role_type, :string
    add_column :users, :college_id, :integer
    add_column :users, :department_id, :integer
  end
end
