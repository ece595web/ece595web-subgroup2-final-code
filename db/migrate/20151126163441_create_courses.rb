class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.integer :college_id
      t.integer :department_id

      t.timestamps null: false
    end
  end
end
