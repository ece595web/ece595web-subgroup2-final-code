class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.text :outcome
      t.text :learning_goals
      t.text :strategic_plan
      t.text :measure_type
      t.text :performance_benchmark
      t.text :data_results
      t.text :data_analysis
      t.text :recommendations
      t.text :met_details
      t.integer :report_id

      t.timestamps null: false
    end
  end
end
