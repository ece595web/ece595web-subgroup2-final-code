class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :college_id
      t.integer :department_id
      t.integer :course_id
      t.integer :user_id
      t.datetime :assessment_start_period
      t.datetime :end_period
      t.datetime :submitted_date
      t.string :status
      t.text :state_all_slos
      t.text :focus_on_slos
      t.text :improvements_on_report_period

      t.timestamps null: false
    end
  end
end
