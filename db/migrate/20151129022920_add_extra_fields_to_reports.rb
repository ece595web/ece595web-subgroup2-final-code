class AddExtraFieldsToReports < ActiveRecord::Migration
  def change
    add_column :reports, :score, :string
    add_column :reports, :comments, :text
  end
end
