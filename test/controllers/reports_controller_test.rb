require 'test_helper'

class ReportsControllerTest < ActionController::TestCase
  setup do
    @report = reports(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reports)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create report" do
    assert_difference('Report.count') do
      post :create, report: { assessment_end_period: @report.assessment_end_period, assessment_measures: @report.assessment_measures, assessment_start_period: @report.assessment_start_period, college_id: @report.college_id, course_id: @report.course_id, data_analysis: @report.data_analysis, data_results: @report.data_results, department_id: @report.department_id, met_details: @report.met_details, performance_benchmark: @report.performance_benchmark, recommendations: @report.recommendations, status: @report.status, student_learning_utcomes: @report.student_learning_utcomes, submitted_date: @report.submitted_date, unm_learning_goals: @report.unm_learning_goals, unm_strategic_plan: @report.unm_strategic_plan }
    end

    assert_redirected_to report_path(assigns(:report))
  end

  test "should show report" do
    get :show, id: @report
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @report
    assert_response :success
  end

  test "should update report" do
    patch :update, id: @report, report: { assessment_end_period: @report.assessment_end_period, assessment_measures: @report.assessment_measures, assessment_start_period: @report.assessment_start_period, college_id: @report.college_id, course_id: @report.course_id, data_analysis: @report.data_analysis, data_results: @report.data_results, department_id: @report.department_id, met_details: @report.met_details, performance_benchmark: @report.performance_benchmark, recommendations: @report.recommendations, status: @report.status, student_learning_utcomes: @report.student_learning_utcomes, submitted_date: @report.submitted_date, unm_learning_goals: @report.unm_learning_goals, unm_strategic_plan: @report.unm_strategic_plan }
    assert_redirected_to report_path(assigns(:report))
  end

  test "should destroy report" do
    assert_difference('Report.count', -1) do
      delete :destroy, id: @report
    end

    assert_redirected_to reports_path
  end
end
